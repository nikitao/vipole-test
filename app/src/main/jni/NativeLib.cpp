#include <jni.h>
#include <string>
#include <sstream>
#include <map>
#include "NativeLib.h"

template <typename T> std::string to_string(T);
std::map<int, int> statusMap;
std::string get_json_string(int, int);

JNIEXPORT jstring JNICALL Java_me_nikitaog_vipoletest_app_TaskTracker_getStatusNative
  (JNIEnv *env, jclass obj, jint id){
      int status = statusMap[id];
      std::string jsonString = get_json_string(id, status);
      statusMap[id] = ++status;
      return env->NewStringUTF(jsonString.c_str());
  }

  JNIEXPORT void JNICALL Java_me_nikitaog_vipoletest_app_TaskTracker_startProcessingNative
    (JNIEnv *env, jclass obj, jint id){
        statusMap[id] = 0;
    }

  JNIEXPORT void JNICALL Java_me_nikitaog_vipoletest_app_TaskTracker_stopProcessingAllNative
    (JNIEnv *env, jclass obj){
        statusMap.clear();
    }

template <typename T> std::string to_string(T value)
{
    std::ostringstream os ;
    os << value ;
    return os.str() ;
}

std::string get_json_string(int id, int status){
    std::string jsonString("{\"id\":\""+to_string(id)+"\",\"status\":\""+to_string(status)+"\"}");
    return jsonString;
}