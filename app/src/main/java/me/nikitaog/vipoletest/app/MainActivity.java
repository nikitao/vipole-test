package me.nikitaog.vipoletest.app;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import me.nikitaog.vipoletest.app.adapters.ItemsAdapter;
import me.nikitaog.vipoletest.app.adapters.PagesAdapter;
import me.nikitaog.vipoletest.app.fragments.DetailsFragment;

public class MainActivity extends ActionBarActivity implements AdapterView.OnItemClickListener {

    private ListView mItemsList;
    private ItemsAdapter mItemsAdapter;
    private boolean mIsTwoPane;
    private ListView mPagesList;
    private PagesAdapter mPagesAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mPagesList = (ListView) findViewById(R.id.pages_list);

        if (mPagesList != null) {
            mIsTwoPane = true;
            setupTwoPaneLayout();
        } else {
            setupOnePaneLayout();
        }
    }

    private void setupOnePaneLayout() {
        mItemsList = (ListView) findViewById(R.id.items_list);
        mItemsAdapter = new ItemsAdapter(this);
        mItemsList.setAdapter(mItemsAdapter);
    }

    private void setupTwoPaneLayout() {
        mPagesAdapter = new PagesAdapter(this);
        mPagesList.setAdapter(mPagesAdapter);
        mPagesList.setOnItemClickListener(this);
        mPagesList.setBackgroundResource(R.drawable.list_shadow);
        mPagesList.performItemClick(null, 0, 0);
    }

    @Override
    protected void onDestroy() {
        if (mItemsAdapter != null) {
            mItemsAdapter.stopProcessingAll();
        }
        super.onDestroy();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        DetailsFragment dFragment = DetailsFragment.newInstance(position);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.details_area, dFragment)
                .commit();
    }
}
