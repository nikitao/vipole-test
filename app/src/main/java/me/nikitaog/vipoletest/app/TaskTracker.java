package me.nikitaog.vipoletest.app;

import android.content.Context;
import android.os.Handler;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

public class TaskTracker {

    public static final int STATUS_COMPLETED = 100;

    private static final String TAG = TaskTracker.class.getSimpleName();
    private static final long QUERY_PERIOD = TimeUnit.MILLISECONDS.toMillis(450);
    private OnStatusUpdatedListener mStatusUpdatedListener;
    private final Map<Integer, TimerTask> mTasks = new ConcurrentHashMap<>();

    private final Timer mTimer = new Timer();

    // handler to publish updates on UI thread
    private final Handler mUiHandler;

    static {
        System.loadLibrary("native");
    }

    public TaskTracker(Context context) {
        mUiHandler = new Handler(context.getMainLooper());
    }

    private static native String getStatusNative(int id);

    private static native void startProcessingNative(int id);

    private static native void stopProcessingAllNative();

    private int getStatus(int id) {
        int status = 0;
        try {
            String output = getStatusNative(id);
            JSONObject json = new JSONObject(output);
            status = json.getInt("status");
        } catch (JSONException e) {
            Log.w(TAG, e);
        }
        return status;
    }

    public void stopProcessingAll() {
        stopProcessingAllNative();

        for (TimerTask task : mTasks.values()) {
            task.cancel();
        }
        mTasks.clear();

        mTimer.purge();
    }

    public void registerTask(final int id) {
        startProcessingNative(id);

        TimerTask tTask = new TimerTask() {
            @Override
            public void run() {
                final int status = getStatus(id);
                if (status == STATUS_COMPLETED) {
                    TimerTask task = mTasks.get(id);
                    task.cancel();
                    mTasks.remove(id);
                }
                if (mStatusUpdatedListener != null) {
                    mUiHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            // there is a pretty small chance, that mStatusUpdatedListener
                            // was set to null by UI thread since last check
                            if (mStatusUpdatedListener != null) {
                                mStatusUpdatedListener.onStatusUpdated(id, status);
                            }
                        }
                    });
                }
            }
        };
        mTimer.scheduleAtFixedRate(tTask, 0, QUERY_PERIOD);
        mTasks.put(id, tTask);
    }

    public void setOnStatusUpdatedListener(OnStatusUpdatedListener listener) {
        mStatusUpdatedListener = listener;
    }

    public interface OnStatusUpdatedListener {
        void onStatusUpdated(int id, int status);
    }
}
