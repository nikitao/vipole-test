package me.nikitaog.vipoletest.app.models;

public class TaskInfo {
    private boolean mIsProcessingStarted;
    private int mStatus;
    private final int mId;

    public TaskInfo(int id) {
        mId = id;
    }

    public int getStatus() {
        return mStatus;
    }

    public void setStatus(int status) {
        mStatus = status;
    }

    public boolean isProcessingStarted() {
        return mIsProcessingStarted;
    }

    public void setProcessingStarted(boolean mIsProcessingStarted) {
        this.mIsProcessingStarted = mIsProcessingStarted;
    }

    public int getId() {
        return mId;
    }
}