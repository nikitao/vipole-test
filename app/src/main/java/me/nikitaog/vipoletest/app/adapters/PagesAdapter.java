package me.nikitaog.vipoletest.app.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import me.nikitaog.vipoletest.app.R;

public class PagesAdapter extends BaseAdapter {

    private static final int PAGE_COUNT = 42;
    private final Context mContext;

    public PagesAdapter(Context context) {
        this.mContext = context;
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.page_item, parent, false);
            ViewHolder holder = new ViewHolder();
            holder.pageDescription = (TextView) convertView.findViewById(R.id.page_description);
            convertView.setTag(holder);
        }
        ViewHolder holder = (ViewHolder) convertView.getTag();
        holder.pageDescription.setText(mContext.getResources().getString(R.string.page_description, position));
        return convertView;
    }

    private static class ViewHolder {
        TextView pageDescription;
    }
}
