package me.nikitaog.vipoletest.app.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import me.nikitaog.vipoletest.app.TaskTracker;
import me.nikitaog.vipoletest.app.R;
import me.nikitaog.vipoletest.app.models.TaskInfo;

public class ItemsAdapter extends BaseAdapter
        implements TaskTracker.OnStatusUpdatedListener {

    private final static int VIEW_TYPES_COUNT = 4;
    private final static int ITEMS_COUNT = 200;
    private final ArrayList<TaskInfo> mData = new ArrayList<>(ITEMS_COUNT);
    private final static long UI_UPDATE_INTERVAL = TimeUnit.SECONDS.toMillis(1);
    private final TaskTracker taskTracker;
    private long lastUpdate;

    private final Context mContext;

    public ItemsAdapter(Context context) {
        mContext = context;
        taskTracker = new TaskTracker(context);
        taskTracker.setOnStatusUpdatedListener(this);

        for (int i = 0; i < ITEMS_COUNT; i++) {
            mData.add(new TaskInfo(i));
        }
    }

    @Override
    public int getItemViewType(int position) {
        return (position / 4) % 4;
    }

    @Override
    public int getViewTypeCount() {
        return VIEW_TYPES_COUNT;
    }

    @Override
    public int getCount() {
        return ITEMS_COUNT;
    }

    @Override
    public Object getItem(int position) {
        return mData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            ViewHolder holder = new ViewHolder();
            switch (getItemViewType(position)) {
                case 0:
                    convertView = LayoutInflater.from(mContext).inflate(R.layout.list_item_1, parent, false);
                    holder.itemDescription = (TextView) convertView.findViewById(R.id.textArea_item_1);
                    holder.processBtn = (Button) convertView.findViewById(R.id.btn_process_1);
                    holder.progressBar = (ProgressBar) convertView.findViewById(R.id.progressBar_1);
                    break;
                case 1:
                    convertView = LayoutInflater.from(mContext).inflate(R.layout.list_item_2, parent, false);
                    holder.itemDescription = (TextView) convertView.findViewById(R.id.textArea_item_2);
                    holder.processBtn = (Button) convertView.findViewById(R.id.btn_process_2);
                    holder.progressBar = (ProgressBar) convertView.findViewById(R.id.progressBar_2);
                    break;
                case 2:
                    convertView = LayoutInflater.from(mContext).inflate(R.layout.list_item_3, parent, false);
                    holder.itemDescription = (TextView) convertView.findViewById(R.id.textArea_item_3);
                    holder.processBtn = (Button) convertView.findViewById(R.id.btn_process_3);
                    holder.progressBar = (ProgressBar) convertView.findViewById(R.id.progressBar_3);
                    break;
                case 3:
                    convertView = LayoutInflater.from(mContext).inflate(R.layout.list_item_4, parent, false);
                    holder.itemDescription = (TextView) convertView.findViewById(R.id.textArea_item_4);
                    holder.processBtn = (Button) convertView.findViewById(R.id.btn_process_4);
                    holder.progressBar = (ProgressBar) convertView.findViewById(R.id.progressBar_4);
                    break;
            }
            assert convertView != null;
            convertView.setTag(holder);
        }

        final ViewHolder holder = (ViewHolder) convertView.getTag();
        holder.itemDescription.setText(mContext.getResources().getString(R.string.item_description, position));
        holder.processBtn.setTag(position);

        TaskInfo tInfo = mData.get(position);
        holder.processBtn.setEnabled(!tInfo.isProcessingStarted());
        if (tInfo.isProcessingStarted()) {
            holder.progressBar.setVisibility(View.VISIBLE);
            holder.progressBar.setProgress(tInfo.getStatus());
        } else {
            holder.progressBar.setVisibility(View.INVISIBLE);
        }

        holder.processBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                int id = (int) v.getTag();
                holder.progressBar.setVisibility(View.VISIBLE);
                v.setEnabled(false);
                taskTracker.registerTask(id);
                TaskInfo tInfo = mData.get(id);
                tInfo.setProcessingStarted(true);
            }
        });
        return convertView;
    }

    @Override
    public void onStatusUpdated(final int id, final int status) {
        TaskInfo tInfo = mData.get(id);
        tInfo.setStatus(status);
        if (status == TaskTracker.STATUS_COMPLETED) {
            tInfo.setProcessingStarted(false);
        }

        //limiting frequent UI updates
        if ((System.currentTimeMillis() - lastUpdate > UI_UPDATE_INTERVAL) || status == TaskTracker.STATUS_COMPLETED) {
            lastUpdate = System.currentTimeMillis();
            notifyDataSetChanged();
        }
    }

    public void stopProcessingAll() {
        taskTracker.stopProcessingAll();
    }

    private static class ViewHolder {
        TextView itemDescription;
        Button processBtn;
        ProgressBar progressBar;
    }
}
