package me.nikitaog.vipoletest.app.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.ListView;
import android.widget.TextView;

import me.nikitaog.vipoletest.app.R;
import me.nikitaog.vipoletest.app.adapters.ItemsAdapter;


public class DetailsFragment extends Fragment {

    private static final String PAGE_ID_ARG = "ItemId";
    private int mPageId;
    private ItemsAdapter mItemsAdapter;
    private ListView mItemsList;

    public static DetailsFragment newInstance(int pageId) {
        DetailsFragment fragment = new DetailsFragment();
        Bundle args = new Bundle();
        args.putInt(PAGE_ID_ARG, pageId);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mPageId = getArguments().getInt(PAGE_ID_ARG);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_detail, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        TextView pageDescription = (TextView) view.findViewById(R.id.page_description);
        pageDescription.setText(getResources().getString(R.string.page_description, mPageId));
        mItemsAdapter = new ItemsAdapter(getActivity());
        mItemsList = (ListView) view.findViewById(R.id.items_list);
        mItemsList.setAdapter(mItemsAdapter);
    }

    @Override
    public void onDestroyView() {
        mItemsAdapter.stopProcessingAll();
        super.onDestroyView();
    }
}
